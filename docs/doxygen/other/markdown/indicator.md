# Indicator Unit

The Model I serves as a water presence indicator. It features dual water presence sensors and a water proof temperature
sensor. This sensor works by measuring how much water the water sensors are detecting. Once both sensors are completely
submerged in water, the unit sends out a message indicating water presence. The unit also features a water proof temperature
sensor to monitor environmental conditions.

## Required Parts

This unit requires the following parts, links to the parts on Amazon.com have been provided. Please note that we DO NOT
get funding from Amazon, they were just the fastest way to get parts for us. Please feel free to explore other sites for
purchasing parts.

* 2 [Water presence sensors](https://www.amazon.com/WINGONEER-Sensor-Droplet-Detection-Arduino/dp/B06XHDZ3Q4/ref=sr_1_fkmr2_1?keywords=arduino+water+presence+sensor&qid=1550608288&s=gateway&sr=8-1-fkmr2)
* 1 [Water proof temperature sensor](https://www.amazon.com/Gikfun-DS18B20-Temperature-Waterproof-EK1083x3/dp/B012C597T0/ref=sr_1_fkmrnull_1_sspa?keywords=arduino+water+presence+sensor&qid=1550608331&s=gateway&sr=8-1-fkmrnull-spons&psc=1)
* 1 [Arduino Uno](https://www.amazon.com/RoboGets-Compatible-ATmega328P-Microcontroller-Electronics/dp/B01N4LP86I/ref=sr_1_1_sspa?keywords=Arduino+uno&qid=1550596021&s=gateway&sr=8-1-spons&psc=1) (A cheaper clone is recommended, like [Elegoo Uno R3](https://www.amazon.com/Elegoo-EL-CB-001-ATmega328P-ATMEGA16U2-Arduino/dp/B01EWOE0UU/ref=sr_1_2_sspa?keywords=Arduino+uno&qid=1550596053&s=gateway&sr=8-2-spons&psc=1))
* 2 [4.7K Ohm resistor](https://www.amazon.com/Projects-25EP5144K70-4-7k-Resistors-Pack/dp/B0185FC5OK/ref=sr_1_2_sspa?crid=3PLHL7BGKMCQZ&keywords=4.7k+ohm+resistor&qid=1550595928&s=gateway&sprefix=4.7k+%2Caps%2C160&sr=8-2-spons&psc=1)
* 1 [DS4201](https://www.amazon.com/Sruik-Tool-DS2401-Silicon-Serial/dp/B079FRMK3D/ref=sr_1_1?keywords=DS2401+Silicon+Serial+Number&qid=1550595888&s=gateway&sr=8-1) silicon serial number
* 1 [NRF24 2.4 GHz radio transceiver](https://www.amazon.com/gp/product/B06VSYJ7HN?pf_rd_p=c2945051-950f-485c-b4df-15aac5223b10&pf_rd_r=G8WTMZ2YE0XCREH1YJRX)


## RF24 Pin Out Table

| Arduino Pin | NRF24 Pin | Wire Color|
|:-----------:|:---------:|:---------:|
|  3.3V       |  VCC      | Red       |
|  GND        |  GND      | Black     |
|   9         |  CE       | Brown     |
|   10        |  CSN      | Yellow    |
|   13        |  SCK      | Orange    |
|   11        |  MOSI     | Gray      |
|   12        |  MISO     | Green     |
|   None      |  IRQ      | N/A       |

## Wiring Diagram

\image html Indicator_bb.png
