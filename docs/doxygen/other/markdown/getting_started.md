# Getting Started

First and foremost, you should setup a [base station](https://wvfloodnet.gitlab.io/Hardware/base_station/). Next,
you should evaluate your needs and determine what combination of sensor nodes will best suite your situation. There
are four supported sensor node configurations:

1. [Water Level Monitor (Model M)](md_docs_doxygen_other_markdown_monitor.html)
2. [Water Presence Indicator (Model I)](md_docs_doxygen_other_markdown_indicator.html)
3. [Soil Saturation Monitor (Model S)](md_docs_doxygen_other_markdown_soil_saturation.html)
4. [Network Range Extender (Model R)](md_docs_doxygen_other_markdown_extender.html)

Each sensor unit has its own purpose. The Model M is intended to monitor how high water has risen in a flood plain. The
Model I is used to monitor if a given location is under water. The Model S monitors soil saturation, and early warning
sign of impending flooding. Lastly, the Model R acts as a range extender for sensor network. It does not feature any 
sensors and only acts as a means to establish a network connection in hard to reach areas. You can download the [sensor
node firmwares here](https://wvfloodnet.gitlab.io/Hardware/downloads/sensor_nodes.zip)

## How Does the Sensor Network Work?

The idea is very simple. Have a bunch of inexpensive sensor nodes collecting data about the
environment. From this collected data, we can monitor for environmental disasters. Since
the sensor nodes are so inexpensive, we can blanket a large area with sensors for a minimal
cost.

## Uploading the Firmware

Once you have constructed your desired node and downloaded the [sensor node firmware](https://wvfloodnet.gitlab.io/Hardware/downloads/sensor_nodes.zip),
you will need to flash the firmware onto the node. To do this, it is recommended that you download either 
[Atom](https://atom.io/) or [Visual Studio Code](https://code.visualstudio.com/) and then install the 
[Platformio](https://platformio.org/) add-on. Your next step is to unzip the sensor node archive and install all
of the libraries in the library folder. This [article](https://community.platformio.org/t/trying-to-understand-how-to-install-custom-local-library/3031)
describes how to install the libraries. Pretty much all you need to do is copy them to the lib folder
of your Platformio home folder. This is usually a hidden folder in your user's home directory. Once the libraries are installed, all you will need to do is plug in your
sensor node, open the firmware you want to upload in Platformio, then click upload. Once the firmware has uploaded
successfully, open the serial monitor to view the output from the sensor node. Here you will see the Node ID, you will
need this serial number to register your device in the next step.

## Register Your Sensor Nodes

To register a new sensor node, you will need to visit [https://wvflood.duckdns.org](https://wvflood.duckdns.org)
and register for an account if you do not already have one. Once you have logged in, click on your name, then go
to the "Asset Management" section and select "Manage Nodes". From here, select "Add New Node" and fill in the
form to add a new node. Once this is complete, your node will be able to attach to any nearby base station and begin
reporting any collected data.


## For Developers

For those interested in developing for the project, there are resources available in the [sensor node firmware](https://wvfloodnet.gitlab.io/Hardware/downloads/sensor_nodes.zip).
In the Arduino folder, there are two folders of interest for you: lib and Sensor Base. The lib folder contains the source
code for the Battery Monitor Library and the Mesh Manager Library written for this project. 

#### Modifying The Project Specific Libraries

To modify the lib and test on
sensor nodes, all you need to do is modify you you Platformio.ini file to include "lib_extra_dirs = absolute/path/to/lib"
where you replace absolute/path/to with the absolute path to where the lib folder is on your system. Next, delete the MeshManager
and Battery Monitor libraries from your Platformio libraries. Restart your Platformio for the changes to take effect. The
sensor node firmware should now compile using the lib folder. You can now make changes to the libraries.

#### Making Your Own Sensor Node

The Sensor Base folder contains a template from which you can make your own sensor node. It contains all of the basic functions
to join the network and send communications to the base station. All you need to do is add your setup code for your sensors
and update the **message** variable in the loop function. You can find examples of how the message should be formatted in the
other sensor node folders or in the [docs](https://wvfloodnet.gitlab.io/Hardware/sensor_nodes/class_mesh_manager.html).

#### The Base Libraries for Wireless Communication

If you are interested in learning about the base libraries, you can find their documentation below. Please note that for
simply extending the platform (e.g. adding a new type of sensor node) it is not necessary to know the NRF libraries as 
the mesh manager acts as an abstraction layer. I would however still encourage you to take a look if you are interested.

1. [NRF24 Library](http://tmrh20.github.io/RF24/])
2. [NRF24Network Library](http://tmrh20.github.io/RF24Network/)
3. [NRF24Mesh Library](http://tmrh20.github.io/RF24Mesh/)

Each of these libraries has an examples folder to help you get started.

Specific versions of these libraries are shipped with the sensor node firmware, please be sure to use theses. Due to the
lack of a Python wrapper for the newer versions of the libraries, older versions were used for compatibility with the 
base station code.

