\mainpage West Virginia Flood Network

# West Virginia Flood Network Sensor Unit Documentation

Welcome to the West Virginia Flood Network Sensor Unit Documentation!

## Objective

The primary objective of this project is to establish a cheap and effective Internet of Things(IoT) enabled sensor
network for flood detection.

## Requirements

This project is licensed under the [GNU GPL3](https://www.gnu.org/licenses/gpl-3.0.en), as such all work contributed
to this project and any extension of this project must be free and open source.

## Acknowledgements

We would like to thank our sponsors for this project, for without their
assistance and support this project would not have been possible

* NASA West Virginia Space Grant Consortium for funding this endeavour
* Marshall University Computer Science Department for providing a workspace
* Dr. Haroon Malik for being our faculty sponsor and for going above and beyond to ensure the project succeeds