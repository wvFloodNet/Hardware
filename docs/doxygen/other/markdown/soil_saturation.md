# Soil Saturation Unit

The Model S serves as a early indicator for pending flood conditions. It monitors the saturation of the soil, when
the soil reaches 100% saturation that is indication that water will begin pooling in the area as the soil can no 
longer absorb any more.

## Required Parts

This unit requires the following parts, links to the parts on Amazon.com have been provided. Please note that we DO NOT
get funding from Amazon, they were just the fastest way to get parts for us. Please feel free to explore other sites for
purchasing parts.

* 1 [Capacitive Soil Saturation Sensor](https://www.amazon.com/Gikfun-Capacitive-Corrosion-Resistant-Detection/dp/B07H3P1NRM/ref=pd_day0_hl_86_1/137-6711179-9144231?_encoding=UTF8&pd_rd_i=B07H3P1NRM&pd_rd_r=f4d31e77-2897-11e9-bd58-413f05c8b3cb&pd_rd_w=0cgDX&pd_rd_wg=y2P2L&pf_rd_p=ad07871c-e646-4161-82c7-5ed0d4c85b07&pf_rd_r=G9965SZ9F6XS8J2JYE36&psc=1&refRID=G9965SZ9F6XS8J2JYE36)
* 1 [Arduino Uno](https://www.amazon.com/RoboGets-Compatible-ATmega328P-Microcontroller-Electronics/dp/B01N4LP86I/ref=sr_1_1_sspa?keywords=Arduino+uno&qid=1550596021&s=gateway&sr=8-1-spons&psc=1) (A cheaper clone is recommended, like [Elegoo Uno R3](https://www.amazon.com/Elegoo-EL-CB-001-ATmega328P-ATMEGA16U2-Arduino/dp/B01EWOE0UU/ref=sr_1_2_sspa?keywords=Arduino+uno&qid=1550596053&s=gateway&sr=8-2-spons&psc=1))
* 2 [4.7K Ohm resistor](https://www.amazon.com/Projects-25EP5144K70-4-7k-Resistors-Pack/dp/B0185FC5OK/ref=sr_1_2_sspa?crid=3PLHL7BGKMCQZ&keywords=4.7k+ohm+resistor&qid=1550595928&s=gateway&sprefix=4.7k+%2Caps%2C160&sr=8-2-spons&psc=1)
* 1 [DS4201](https://www.amazon.com/Sruik-Tool-DS2401-Silicon-Serial/dp/B079FRMK3D/ref=sr_1_1?keywords=DS2401+Silicon+Serial+Number&qid=1550595888&s=gateway&sr=8-1) silicon serial number
* 1 [NRF24 2.4 GHz radio transceiver](https://www.amazon.com/gp/product/B06VSYJ7HN?pf_rd_p=c2945051-950f-485c-b4df-15aac5223b10&pf_rd_r=G8WTMZ2YE0XCREH1YJRX)


## RF24 Pin Out Table

| Arduino Pin | NRF24 Pin | Wire Color|
|:-----------:|:---------:|:---------:|
|  3.3V       |  VCC      | Red       |
|  GND        |  GND      | Black     |
|   9         |  CE       | Brown     |
|   10        |  CSN      | Yellow    |
|   13        |  SCK      | Orange    |
|   11        |  MOSI     | Gray      |
|   12        |  MISO     | Green     |
|   None      |  IRQ      | N/A       |

## Wiring Diagram

\image html Soil_Saturation_bb.png
