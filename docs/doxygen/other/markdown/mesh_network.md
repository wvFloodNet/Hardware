# What is a Mesh Network?

As defined by Wikipedia, a mesh network is a local network topology in which the infrastructure nodes 
(i.e. bridges, switches and other infrastructure devices) connect directly, dynamically and non-hierarchically
to as many other nodes as possible and cooperate with one another to efficiently route data from/to clients. 

\image html mesh_structure.png

What does that mean? In short, it means that everything on the network is connected to everything else, meaning that the
range of the network is extended by every connected device. The devices can relay messages between each other until they
reach the device they were meant for. This can be though of a WiFi range extender in a way, expect that every device that
connects to the WiFi extends the range.

# How Ours Works

Ours is not a true mesh network in the sense that every device talks to every device. Ours follows a tree structure,
that is one device will talk its children and its parent device directly. That device will not talk to its sibling
devices directly nor will it talk to its grandchildren devices or grandparent devices. The structure is illustrated 
in the figure below.

\image html tree_structure.png

Siblings are the same color and arrows show the lines of communication, the arrows point to the nodes parent.

Although the implementation structure is not a true mesh network, for all intents and purposes it functions the same. To
learn more on the structure of the network, please visit the [RF24Network](http://tmrh20.github.io/RF24Network/) 
documentation. It describes in detail the structure and how the implementation of the network works. 

The library we use to handle our mesh network [RF24Mesh](http://tmrh20.github.io/RF24Mesh/) builds upon the
[RF24Network](http://tmrh20.github.io/RF24Network/) library and mimics the [Zigbee protocol](https://www.zigbee.org/zigbee-for-developers/network-specifications/).

# The Mesh Manager

As an abstraction layer to RF24 libraries that are we, we have written a [Mesh Manager Class](class_mesh_manager.html), 
this class is responsible for handling all interactions with the mesh network for the user. This enables developers to
focus more on how the sensor node should interact with the environment and less on how to interact with the base station.