.. Name documentation master file, created by
   sphinx-quickstart on Thu Nov  8 22:42:01 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


West Virginia Flood Network Base Station Documentation
======================================================

Welcome to the West Virginia Flood Network Base Station Documentation!

Objective
---------

This project aims to create a sustainable IoT enable flood detection platform using open source technologies.
The ultimate goal for this project is to serve as a crowd source data collection platform, that is to create
a community of home owners, local governments, and other interested parties who deploy the system to create a
wide scale flood detection network.

Requirements
------------

There is no special knowledge or skills needed to deploy a sensor network. All hardware designs and software
are open source, so all the user needs to do is purchase the necessary hardware and follow the instructions on how
to assemble the sensors and base station. All of the software needed to install the system is freely available
and automates most of the process. The base station does require the following:

1. A constant power supply
2. A network connection

How these requirements are satisfied has been left up to the user. The power could be from a wall outlet, a battery,
or whatever other means the user wishes to use. Same for the network connection, it could be a wired connection, WiFi,
or even a cellular connection if the user so chooses.

Acknowledgements
----------------

We would like to thank our sponsors for this project, for without their
assistance and support this project would not have been possible

* NASA West Virginia Space Grant Consortium for funding this endeavour
* Marshall University Computer Science Department for providing a workspace
* Dr. Haroon Malik for being our faculty sponsor and for going above and beyond to ensure the project succeeds


.. toctree::
   :maxdepth: 2
   :caption: Contents: