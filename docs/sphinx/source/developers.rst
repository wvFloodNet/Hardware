For Developers
==============

Development of this package requires you to have a Raspberry Pi and either physical or remote access to it.
You will not be able to edit run this code on your computer directly. Instead, you have a few options. You will also
need to install two requirements to develop this project:

Remote Python Interpreters
^^^^^^^^^^^^^^^^^^^^^^^^^^
This was my preferred method, but everyone has their own style. This method allows you to develop own your machine
and execute the code remotely. The Pycharm Python IDE supports this feature if you have the professional version. `Here <https://www.jetbrains.com/help/pycharm/configuring-remote-interpreters-via-ssh.html>`_ is a guide on how to set that up.
I am sure there are other IDEs that support this feature, but I use Pycharm, so if you would like to use another IDE
you will need to figure out how to set this up on your own.

Deploy and Test
^^^^^^^^^^^^^^^
For this method, you can develop the code on your own machine and then deploy the package over SFTP or FTP to the Pi.
From there you can use the command line or whatever tool you choose to execute and test the code. This is a pretty straight
forward method, but your local machine will be little to no help when it comes to autocomplete.

Develop Directly on the Pi:
^^^^^^^^^^^^^^^^^^^^^^^^^^^
This method is probably the most direct, but the Raspberry Pi is not capable of supporting more advanced IDEs (such as
Pycharm) easily, resulting in slow run times. There are a few built in IDE's that come with Raspberry Pi Desktop.

Setting up the Development Environment
--------------------------------------
The best way to setup the dev environment is to run the installer script the comes with the base station software.
This will handle setting up all the dependencies and all of that. You will need to disable the basestation system service
in order to run your own builds. To do that, execute the following in the shell:

.. code-block:: shell

    sudo systemctl stop basestation    # stops the service now
    sudo systemctl disable basestation # prevents the service from starting on reboot

You are now free to run your modified code on the Pi!

If you ever want to re-enable the service:

.. code-block:: shell

    sudo systemctl enable basestation # enables service start at reboot
    sudo systemctl start basestation  # starts the service now

Please note that re-enabling the service with run the code installed with the installer, not your modified code. You will
need to repackage your code using the basestation_bundler.sh script included in the Gitlab repo. It will build the installer
package for you.