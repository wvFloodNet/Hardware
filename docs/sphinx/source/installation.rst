
============
Installation
============

Below are the supported platforms, required software, and instructions for installing
and setting up the base station.

Platforms
=========
This project was built and tested on the Raspberry Pi 3 Model B+, in theory it should
run on any of the newer RPis though.

Requirements
============

* `Raspbian Stretch Lite <https://downloads.raspberrypi.org/raspbian_lite_latest>`_
* Python 3.5 (This should be included in Raspbian)
* `Base Station software bundle <https://wvfloodnet.gitlab.io/Hardware/downloads/base_station.zip>`_

Other Materials
===============
You may need the following physical materials if you do not wish to connect remotely to the base station for setup:

* A monitor
* An HDMI cable
* A keyboard

Install
=======

Burning the Disk Image
----------------------
After downloading the Raspbian image, you may follow the `official installation guide <https://www.raspberrypi.org/documentation/installation/installing-images/>`_
from the `Raspberry Pi website <https://www.raspberrypi.org/>`_. Please skip down to the "Writing an image to the SD Card"
section. Once you have done this, while the SD card is still in your computer, mount the SD card.

Setting Up
----------
If you plan on using a wired internet connection, you may skip the next step. If you plan on using a dedicated monitor
for the base station and do not plan to access it remotely, you may skip the next two steps. You will see two new
devices, one called boot and another called rootfs.

1.Setting Up With WiFi
^^^^^^^^^^^^^^^^^^^^^^
If you would like to connect the base station
to your WiFi, you will need to access the boot device and make a text file called "wpa_supplicant.conf". In the
newly created file
please copy the follow, replacing the ssid and password with the ssid and password of your own network. You will
also ned to insert your country code (if you are in the United States, the code is US)

.. code-block:: text

    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1
    country=your_ISO-3166-1_two-letter_country_code

    network={
        ssid="you_SSID"
        psk="your_password"
        key_mgmt=WPA-PSK
    }

Please note that this method will not work as is for enterprise networks, you will need to contact your network
administrator for help setting up the device.


2.Setting Up SSH (remote access)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
SSH (Secure SHell) is a secure remote access protocol. If you are a windows user and would like to use this feature,
you will need to install an SSH Client such as `PuTTy <https://www.putty.org/>`_, Mac and Linux users will already have
SSH as part of their systems. To enable SSH on the base station, simply create a empty text file called "ssh". That is
all that is required.

3.Preparing for the First Boot
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You should have already downloaded a copy of the `Base Station software bundle <https://wvfloodnet.gitlab.io/Hardware/downloads/base_station.zip>`_
, if you have not please do so now
by clicking the link. Now open the rootfs device from the SD card, and navigate to the /home/pi folder. Place the
base_station.zip file there. Now eject the SD from your machine and place it in the Raspberry Pi.

4.The First Boot
^^^^^^^^^^^^^^^^
Now that the SD card is in your RPi, go ahead and connect any peripherals you need (the monitor and keyboard if you
are using them) and then connect your RPi to power. If you are remotely connecting to the RPi, you will need to find your
device's IP address. This can be done by either seeing what devices are on your network by viewing your WiFi routers
admin page, or by using a program such as `nmap <https://nmap.org/>`_. Once you have your device's IP, connect to the device
using the username "pi" and the password "raspberry".

Once you are connected to the RPi, either through wireless or via a connected monitor, you should see a text interface.
This is called a command line. From here we are going to execute a few simple commands and then everything will be setup
and ready to go. First off, enter the command

.. code-block:: shell

    sudo raspi-config

This will bring up the RPi's configuration menu. Here is where you should change your password and localization options.
There are plenty of `guides <https://www.raspberrypi.org/documentation/configuration/raspi-config.md#change-timezone>`_
on how to do this, so we will direct you to the official Raspberry Pi site once again. Following this guide, you will
be able to customize the base system to fit your needs.

Once you have made the desired changes, go the the interface options menu of the raspi-config editor and enable SPI
and I2C. To apply all changes, you will need to reboot the RPi.

.. code-block:: shell

    sudo reboot

Once the RPi has restarted, type the following command:

.. code-block:: shell

    ls

This will list everything in the present working directory, A.K.A where you are in the file system. You should see
a listing for one file, the base_station.zip file.

**Prior to running the installer, you should register for a DarkSky API Key**. At the end of the installation,
the script will ask you for your API key, to get one follow this `link <https://darksky.net/dev>`_. The key is free and
allows you to make up to 1000 API calls per day. Your base station will check the weather ever 15 minutes, making 96
calls per day. This would allow you to run 10 base stations per API key. To install the software, execute the following
commands and then wait.



.. code-block:: shell

    unzip base_station.zip   # this will unzip the the files
    cd base_station          # this puts us in the newly unzipped files
    sudo ./install.sh        # this will update the RPi software and install the base station package

The installation does take some time, so please be patient. This could take as little as 3 minutes or as long as 10
depending on internet speeds and model of RPi.



5.Validating the Installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Now that you have installed the software, please reboot your RPI. Once the system has restarted,
please type the following command to see if the system is working:

.. code-block:: shell

    sudo systemctl status basestation

You should see a message output that shows a green dot and says the service is running. You will also see the most
recent output of the program. If the service is running and is reporting no errors, then your installation was a success
and your are ready to take part in the West Virginia Flood Network!


6. Registering the Base Station
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To register your base station, please visit `https://wvflood.duckdns.org <https://wvflood.duckdns.org>`_ and create an
account. Once your account is created, click on your name in the top right corner. On the menu that appears, under "Asset Management",
select "Manage Base Stations". On the next screen, select "Add New Base Station". From there, all you will need to do is
fill out the form. To locate your base station serial number, enter the following in the terminal on the base station:

.. code-block:: shell

    sudo journalctl -u basestation

This will show you the log output from the base station. You should easily be able to location the serial number. If you
cannot find the serial number, enter this command in the terminal:

.. code-block:: shell

    sudo systemctl restart basestation; sudo journalctl -u basestation

This will restart the base station service, resulting in the serial number being printed to the log again. To find it,
you will need to scroll to the bottom of the log using the page down button.


Trouble Shooting
================
There are a couple of common issues, here are the most common and how to solve them

1.No WiFi
---------
There are two common reasons that the WiFi is not working:

1. There was a typo in setting up the WiFi Network
2. Your Router has MAC authentication on it

The solve the first issue, you will need to edit the /etc/wpa_supplicant/wpa_supplicant.conf file and supply the correct
information. This can be done by either removing the SD card, mounting rootfs, and editing the file on another machine or
by using the nano program on the RPi itself. To use nano, do the follwing:

.. code-block:: shell

    sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

This will bring up a simple text editor. To save your changes, hit ctrl-o. To exit, hit ctrl-x.

The second issue is less common, as this option is usually not enabled by default on most routers. You will need to consult
your router's user manual for either added your RPi to the accepted MAC list or disabling MAC authentication.

2.The Service Reports a Radio Hardware Error
--------------------------------------------
This is usually an issue with how the radio was wired. Either the wrong pins were connected or the
wires used to connect the radio are bad. Double check to ensure that all the pins from the radio are wired to the
proper pins on the RPi. If the wiring is okay, you may want to run a continuity test on the individual wires using
a multimeter. If the wires check out and the radio is wired properly, you may have a bad radio.

Is Your Issue not Listed?
-------------------------
This project is still in it's infancy, so there are most likely problems that will occur that are not yet
listed. Feel free to report and issue on our Gitlab page (link to be added later)


.. toctree::
   :maxdepth: 2
   :caption: Contents: