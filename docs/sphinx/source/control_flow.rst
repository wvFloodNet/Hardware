
.. Reference Name

.. _control_flow:

Base Station Control Flow
=========================

The control flow attempts to conserve the batteries of the sensor nodes by only
having the monitor the environment when there are possible flooding conditions.
The logic in this diagram in implemented in the baseStation module.
Every 15 minutes the script will check for the following conditions using the `Darksky weather API <https://darksky.net/poweredby/>`_:

* It is about to rain in the next hour
* It is currently raining
* A flood warning has been issued

These conditions may be expanded to include more items in the future, but in this stage in the project this is
sufficient for testing. The method that determines whether or not there are flooding conditions can be found in
base_station.utilities, it is called `possible_flooding <base_station.html#base_station.utilities.possible_flooding>`_.

Figures
-------

.. figure:: _static/control_flow.png
   :align: center

   Figure 1: Logic Control Flow


.. toctree::
   :maxdepth: 2
   :caption: Contents:
