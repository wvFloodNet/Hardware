Modules
=======

The base_station package includes three submodules, for more detail please view the package documentation.
The baseStation script is the main program and implements the control flow illustrated on this `page <control_flow.html>`_.
Upon installing the entire package with pip, this script is made executable and added to the system path.

.. toctree::
   :maxdepth: 4

   base_station