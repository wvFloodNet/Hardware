
.. Reference Name

.. _wiring_guide:

Wiring Guide
============
The wiring guide is for attaching the NRF24 radio to the Raspberry Pi.
The NRF24 is a rather inexpensive radio and can be purchased for about 6$ on Amazon.
This radio operates on the same band and most common WiFi routers. Below are a table of
the pin outs and a diagram of the wired unit. Do not be alarmed if your NRF24 does not look
like the one in the diagram below, this is a generic NRF24 used for the diagram. There are a few
varieties of the radio, we would suggest one that is capable of supporting an attachable antennae.

Recommended Radio
-----------------
Although any NRF24 should work, we will recommend `this one <https://www.amazon.com/kuman-nRF24L01-Antenna-Wireless-Transceiver/dp/B06VSYJ7HN/ref=sr_1_2?keywords=nrf24&qid=1550516379&s=gateway&sr=8-2>`_
as it is the radio we used for testing. There are cheaper variants of this radio, but they do not have the option
to attach a larger antennae. Depending on the needs of the user, the other radios may or may not be more suitable.

Tables
------

Table 1: Pin Outs

+-------------+---------+--------------+
| RPi Pin     | NRF Pin |  Wire Color  |
+=============+=========+==============+
|   3.3V (1)  |   V+    |   Red        |
+-------------+---------+--------------+
|   GND (6)   |  GND    |  Black       |
+-------------+---------+--------------+
|  GPIO8 (24) |  CSN    | Yellow       |
+-------------+---------+--------------+
| GPIO22 (15) |   CE    |  Blue        |
+-------------+---------+--------------+
|   MOSI (19) |  MOSI   |  Green       |
+-------------+---------+--------------+
|   SCKL (23) |  SCK    | Orange       |
+-------------+---------+--------------+
|  Not Wired  |  IRQ    |  None        |
+-------------+---------+--------------+
|   MOSI (21) |  MISO   |  White       |
+-------------+---------+--------------+



Figures
-------

.. figure:: _static/base_station_bb.png
   :align: center

   Figure 1: Base Station Wire Guide

.. toctree::
   :maxdepth: 2
   :caption: Contents: