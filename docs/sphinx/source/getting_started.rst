Getting Started
===============

This guide is for the use and development of the base station hardware and software. This guide is for anyone who is interested
in taking part in the `West Virginia Flood Network <https://wvflood.duckdns.org/>`_ and contributing to the data collected
as well as expanding the sensor
network. This guide comes in two parts: setting up the base station (this guide), and the
`sensor node guide <https://wvfloodnet.gitlab.io/Hardware/sensor_nodes/>`_.

This guide features 4 primary parts:

1. Wiring Guide
2. Installation
3. Control Flow Diagram
4. Modules (Developer Documentation)



