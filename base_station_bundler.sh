#!/usr/bin/env sh
# Packaging script, creates a zip file for deploying code to end user for the Raspberry Pi.
stamp=$(date +"%Y-%m-%d_%H-%M-%S")

mkdir base_station

# building newest python package
cp -r libraries base_station/                               # includes libraries to compile and compilation scripts
cd ./src/RPi/
pip3 install setuptools                                     # ensures that we have the python packaging tools
python3 setup.py sdist                                      # builds base station Python package
rm -r *.egg-info                                            # src dir cleanup
cd ../../
mv ./src/RPi/dist/*.gz base_station/
rm -r ./src/RPi/dist/                                       # src dir cleanup

# building readme
echo "Packaged on $stamp" > base_station/readme.txt
echo "Be sure to run the installer from inside this directory" >> base_station/readme.txt

# final packaging
mv -v base_station/libraries/scripts/install.sh base_station/  # adds automated installer to the package
cp -v ./src/RPi/logging_config.ini base_station/               # adds the logger config
cp -v ./src/RPi/config.ini base_station/                       # adds base station config file
zip -r base_station.zip base_station/*                         # creates package archive
rm -r base_station                                             # cleanup