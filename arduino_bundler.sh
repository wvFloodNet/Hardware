#!/usr/bin/env sh
# Packaging script, creates a zip file for deploying code to end user for the sensor nodes (Arduinos).
stamp=$(date +"%Y-%m-%d_%H-%M-%S")

mkdir sensor_nodes

# copying arduino files and libraries
cp -r libraries/src sensor_nodes/libraries              # includes libraries to compile and compilation scripts
cp -r src/Arduino/  sensor_nodes                        # including Arduino source code

# packaging the Custom Libraries
cd sensor_nodes/Arduino/lib/
zip -r MeshManager.zip MeshManager/
zip -r BatteryMonitor.zip BatteryMonitor
mv MeshManager.zip BatteryMonitor.zip ../../libraries
cd ../../../

# building readme
echo "Packaged on $stamp" > sensor_nodes/readme.txt

# final packaging
zip -r sensor_nodes.zip sensor_nodes/*                  # creates package archive
rm -r sensor_nodes                                      # cleanup