/**
 *  \section Introduction 
 *        This unit has two ultrasonic sensors that are used to determine how high the water level of a monitored location is.
 *        This unit also have a humidty and temperature sensor on it so that it may be used to monitor other aspects of the 
 *        environment it occupies. This unit was designed to be placed over a flood prone area high above the ground so that
 *        it may monitor rising water levels.
 *        
 *  \author Patrick Shinn
 */

#include <Arduino.h>
#include <RF24Network.h>
#include <MeshManager.h>
#include <Settings.h>
#include <DHT.h>

// ultrasonic sensor setup
const int trig = 4;
const int echo = 3;
const int trig1 = 2;
const int echo1 = 6;
long distance, distance1; // data variables
long startDistance1, startDistance2;
int obs = 0, avgStart = 0, retry = 500, checkHumidity = 0;

//hum-temp sensor
DHT dht;
int dhtPin = 7;
float humidity = 0, temp = 0;

// sensor node 
uint32_t displayTimer = 0;  // clock
bool sleep = false;         // sleep state of the node
String message;             // message placeholder.
MeshManager mesh;           // handles all mesh communications


// prototype functions, defined in helper function section below the loop function.
String messageBuilder(int us1, int us2, int start, float tp, float hm);
int ultraSensor(int trigPin, int echoPin);

/**
 * The setup function is responsible for initializing tha sensor unit.
 * Here we setup the radio, connect to the mesh, and setup the 
 * sensors. For this model, we also establish the baseline for
 * measuring how high the water has risen by taking distance samples.
 */
void setup() {
  Serial.begin(115200);
  Serial.println("Starting Model M...");

  // ultSensor setup
  pinMode(trig, OUTPUT); // sets trig pin to output
  pinMode(echo, INPUT);   // sets echo pin to be input
  pinMode(trig1, OUTPUT);
  pinMode(echo1, INPUT);

  // temp-hum setup
  dht.setup(dhtPin, dht.DHT22);

  // getting a good average start distance
  Serial.println("Getting distance:");
  int data[10];
  int runs = 10;
  int outlierThreshold = 20;
  for(int i =0; i < runs; i ++){
    int sum = -1;
    while(sum < 0){ // case in which the sensor reading is a fluke
      delay(200);
      int sens1, sens2;
      sens1 = ultraSensor(trig, echo);
      delay(200);
      sens2 = ultraSensor(trig1, echo1);

      Serial.print("Sensor 1 reports: ");
      Serial.println(sens1);
      Serial.print("Sensor 2 reports: ");
      Serial.println(sens2);

      sum = sens1 + sens2;
      sum = sum / 2; // averaging the sensor readings
    }

    data[i] = sum;
    Serial.println("AVG: " + String(sum));
    delay(1000);
  }

  // now we need to error check the data and ensure we eliminate any outliers the best we can.
  bool outlier = true;
  while(outlier){
    outlier = false;
    for(int i=0; i < runs; i ++) avgStart += data[i];
    avgStart /= runs; // averaging
    Serial.println("Outlier detection value: " + String(avgStart));
    for(int i=0; i < runs; i ++){
      if(abs(data[i] - avgStart) > outlierThreshold){
        Serial.println("outlier detected: " + String(data[i]));
        data[i] = avgStart;
        outlier = true;
      }
    }
     avgStart = 0;  // reseting average start value
  }
  // final summing of the data and averaging it.
  for(int i=0; i < runs; i ++) avgStart += data[i];
  avgStart /= runs; // average the data.
  Serial.println("Final Distance: " + String(avgStart));  

  mesh.begin(5); //  ###### IMPORTANT, BE SURE TO REPLACE "B" WITH YOUR OWN MODEL CODE ######
  displayTimer = millis(); // ensures we have delay before our first sensor data transmission.
}

/*
 * Main function, collects sensor data and transmits it to the base station.
 */
void loop() {
  // refeshes mesh structure
  mesh.update();

  if(!sleep){
    // if the node is awake, collect and transmit data
    if (millis() - displayTimer >= wait) { // keeps the arduino alive to relay messages 
      displayTimer = millis();
  
      // getting temp-hum
      temp = dht.getTemperature();
      humidity = dht.getHumidity();
       
      // getting ultrasensor distance
      distance = ultraSensor(trig, echo);
      delay(200);
      distance1 = ultraSensor(trig1, echo1);
      
      // preparing message for sending
      message = messageBuilder(distance, distance1, avgStart, temp, humidity);
      mesh.transmit('M', message);
    }
  }else{
    // if we are not actively transmitting, then we need to send a heartbeat
    if(millis() - displayTimer >= heartBeatTime){
      displayTimer = millis();
      mesh.heartbeat();
    }
  }

  // listening for sleep and wake messages from the base station.
  while(mesh.available()){
   RF24NetworkHeader header;
      unsigned int data;
      mesh.read(header, &data, sizeof(32));
      Serial.print("Message type: ");
      Serial.println((char)header.type);
      if((char)header.type == 'S'){
        sleep = true;     // sleep message recieved
        Serial.println("Sleeping node...");
      }
      else if((char)header.type == 'W'){
        sleep = false; // wake message recievd
        Serial.println("Waking node...");
      }
  }
}

// ######################################## Helper Functions ############################################################

/**
 * Reads values from a given ultra sensor
 * 
 * @param trigPin 
 *        int pin value of the trigpin
 * @param echoPin 
 *        int pin value of the echopin
 * @return int measured distance value
 */
int ultraSensor(int trigPin, int echoPin){
  int duration;
  digitalWrite(trigPin,HIGH);
  delay(10); // sends a pulse for this duration
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  return duration*0.034/2; // magic number for calculating distance based on time it takes for the signal to bounce back.
}

/**
 * Constructs the string message to be sent over the radio.
 * 
 * @param us1 
 *        int value from the first ultra sensor
 * @param us2 
 *        int value from the second ultra sensor
 * @param start 
 *        int value of starting distance measured by the ultra sensors
 * @param tp 
 *        float value of the measured temperature in celcius
 * @param hm 
 *        float value of the measured humidity
 * @return String message
 */
String messageBuilder(int us1, int us2, int start, float tp, float hm){
  
  String message = "";
  String err;

  // ultra sensor handling.
  if(abs(us1 - us2) < 10){ // if the differnce in the sensor values is less than 10 cm, just average them and send it in the message.
    int distance = (us1+us2)/2;
    int finalDist = start - distance;
    if(finalDist < 0 && finalDist > -10) finalDist = 0; // in the event we get a minor negative number, ignore it. Sensor error.
    else if(finalDist < -10) finalDist = -1;
    message += "us";
    message += finalDist;
    message += ",";
    obs = 0;
  }
  else{  // otherwise we need to check for hardware failure
    if(us1 == 0 || us2 == 0) err += ",er1"; // if either value is 0, hardware failure.
    else{ // we just have something obstructing the sensors.
      obs ++;
      if(obs >= 2) err += ",er2";
    }
  }

  // temp and humidity
  if(isnan(tp)) err += ",er3"; // if we get a non number, send an error message
  else{
    message += "tp"; // else insert the data
    message += String(tp, 2);
  }
  if(isnan(hm)) err += ",er4"; // repeat from before.
  else{
    message += ",hm";
    message += String(hm, 2);
  }

  // final string build
  message += err;
  return message;
}