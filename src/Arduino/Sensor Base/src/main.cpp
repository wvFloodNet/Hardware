/**
 *  \file
 *  \section Introduction
 *        This is the base code for all sensor units. To make your own, simply copy this file then make any changes
 *        that you need.
 *
 *  \author Patrick Shinn
 */

#include <Arduino.h>
#include <RF24Network.h>
#include <MeshManager.h>
#include <Settings.h>


// Insert your global variables  here
//#############################

//#############################

uint32_t displayTimer = 0;  // clock
bool sleep = false;         // sleep state of the node
String message;             // message placeholder.
MeshManager mesh;           // handles all mesh communications

/* 
 * Declare any prototype fucntions you need here, it is recommended that you actually write the function
 * in the section below the loop function. This will help keep the code organization similar to the 
 * other already written firmware.
 */

// insert your prototype functions here
//#############################

//#############################

/**
 * The setup function adds the node to the mesh and requests a new Node ID.
 * This is the base model, so there is no other functionality.
 */
void setup(void){
   // start serial port for debugging
  Serial.begin(115200);
  Serial.println("Starting");

  // insert your setup code here
  //#############################

  //#############################  


  mesh.begin(5);
  displayTimer = millis(); // ensures we have delay before our first sensor data transmission.
}

/**
 * The Main function relays messages from other nodes as well as
 * reports it's own heartbeats.
 */
void loop() {
  mesh.update();

  if(!sleep){
  // Send sensor data to the master node while this node is awake.
    if (millis() - displayTimer >= wait) {
      displayTimer = millis();

      // insert your code here
      //#############################

      //#############################

      mesh.transmit('M', message);
    }
  }else{
    // This section relays heartbeat information while the node is sleeping.
    if(millis() - displayTimer >= heartBeatTime){
      displayTimer = millis();
      mesh.heartbeat();
    }
  }

  // listening for sleep and wake messages from the base station.
  while(mesh.available()){
    Serial.println("Message received!");
     RF24NetworkHeader header;
        unsigned int data;
        mesh.read(header, &data, 32);
        Serial.print("Message type: ");
        Serial.println((char)header.type);
        if((char)header.type == 'S'){
          sleep = true;     // sleep message received
          Serial.println("Sleeping node...");
        }
        else if((char)header.type == 'W'){
          sleep = false; // wake message received
          Serial.println("Waking node...");
        }
  }
}

// ######################################## Helper Functions ############################################################

// insert your code here
//#############################

//#############################