/**
 *  \file
 *  \section Introduction
 *        This unit monitors the soil saturation. 
 *
 *  \author Patrick Shinn
 */

#include <Arduino.h>
#include <RF24Network.h>
#include <MeshManager.h>
#include <Settings.h>



// soil saturation sensor setup
int dry = 570, saturated = 275;
float saturation, range;

uint32_t displayTimer = 0;  // clock
bool sleep = false;         // sleep state of the node
String message;             // message placeholder.
MeshManager mesh;           // handles all mesh communications


/**
 * The setup function adds the node to the mesh and requests a new Node ID.
 * This is the base model, so there is no other functionality.
 */
void setup(void){
  
  // start serial port for debugging
  Serial.begin(115200);
  Serial.println("Starting");

  // soil saturation range setup
  range = dry - saturated;
 
  mesh.begin(5);
  displayTimer = millis(); 
}


/**
 * The Main function relays messages from other nodes as well as
 * reports it's own heartbeats.
 */
void loop() {

  mesh.update();

  if(!sleep){
  // Send sensor data to the master node while this node is awake.
    if (millis() - displayTimer >= wait) {
      displayTimer = millis();

      float val = analogRead(5); // gets a reading from the soil saturation sensor
      int saturationPercent;   // we will use this to truncate the decimal in the end

      val -= saturated; // puts our value on the same scale as our range

      if (val < 0) val = 0; // accounts for difference in range of sensors
      if (val > range) val = range;

      saturation = 1.00 - (val/range);

      saturationPercent = saturation * 100; // convert to percent
      
      Serial.print(saturationPercent); 
      Serial.println("%");

      message = "ss";
      message += saturationPercent;

      mesh.transmit('M', message);
    }
  }else{
    // This section relays heartbeat information while the node is sleeping.
    if(millis() - displayTimer >= heartBeatTime){
      displayTimer = millis();
      mesh.heartbeat();
    }
  }

  // listening for sleep and wake messages from the base station.
  while(mesh.available()){
    Serial.println("Message received!");
     RF24NetworkHeader header;
        unsigned int data;
        mesh.read(header, &data, 32);
        Serial.print("Message type: ");
        Serial.println((char)header.type);
        if((char)header.type == 'S'){
          sleep = true;     // sleep message received
          Serial.println("Sleeping node...");
        }
        else if((char)header.type == 'W'){
          sleep = false; // wake message received
          Serial.println("Waking node...");
        }
  }
}