#include "MeshManager.h"

String MeshManager::getHex(byte b)
{
    String hex = String(b/16, HEX);
    hex += String(b%16, HEX);
    return hex;
}

String MeshManager::getIdentifier(int DSpin){
    OneWire ds(DSpin);
    String identifier;

    //1-Wire bus reset, needed to start operation on the bus.
    ds.reset();
    ds.write(0x33);  //Send Read data command
    ds.read(); // disregard family value
    for (int i = 1; i <= 6; i++) identifier += getHex(ds.read());
    return identifier;
}

void MeshManager::releaseAddress(){
    if(!mesh.releaseAddress()){
        while(true){
        bool done = mesh.releaseAddress();
        if(done) break;
        }
    }
}

void MeshManager::getAddress(){
    // Getting a unique node id
    Serial.print("Current Node ID: ");
    Serial.println(mesh.getNodeID());
    Serial.println("Waiting for address..");

    long timer = millis(); // addressing timer

    while(true) { // here we wait until we get a message from the pi
        // checking to see if we have waited more than 5 minutes for an address
        if (millis() - timer > 30000){
            Serial.println("Failure threshold reached, restarting.");
            delay(500);
            restart();
        }

        mesh.update();
        bool addressed = false;
        while (network.available()) {
            RF24NetworkHeader header;
            unsigned int address;
            network.read(header, &address, 32);
            Serial.print("Message type: ");
            Serial.println((char)header.type);
            addressed = true;
            // now that we have a new address, release the ip assigned to 255 and assign our new node ID
            releaseAddress();
            mesh.setNodeID(address);
            Serial.print("New Node ID: ");
            Serial.println(mesh.getNodeID());
            if (addressed) break;  // prevents double addressing
        }
        if(addressed) break; // ends the address waiting loop
    }
    Serial.println("Releasing current address...");
    while(!mesh.renewAddress()){ // request a new IP, fails after 5 minutes.
        fail_count ++;
        if (fail_count > 9) {
            Serial.println("Failure threshold reached, restarting.");
            delay(500);
            restart(); // if we fail to renew 10 times, restart the node.
        }
    }; 
    fail_count = 0; // reset fail counter.
    mesh.update();
    Serial.println("Done!");
}

void MeshManager::begin(int IDpin){
    arduinoID = getIdentifier(IDpin); // gets a unique ID from the onboard DS4201

    Serial.print("Node ID: ");
    Serial.println(arduinoID);

    // rf24 setup
    mesh.setNodeID(nodeID); // Set the nodeID manually
    Serial.println("Connecting to the mesh...");
    mesh.begin();           // init mesh network

    bool sent = false;
    while(!sent){
        sent = heartbeat();
        if(sent) break; // skip the delay
        delay(500); // delay so we don't flood the air with signals
    }
    getAddress();
    Serial.println("Connection established!");
}

int MeshManager::getNodeID(){
    return nodeID;
}

bool MeshManager::available(){
    return network.available();
}

void MeshManager::update(){
    mesh.update();
}

void MeshManager::read(RF24NetworkHeader& header,void * message, int size){
    network.read(header, message, size);
}

bool MeshManager::heartbeat(){
    Serial.println("Heartbeat");
    bool sent = false;
    while(!sent){
        sent = transmit('H', "");
        if(sent) break;
        delay(500);
    }
    return sent;
}

bool MeshManager::heartbeat(int batteryPercent){
    Serial.println("Heartbeat");
    Serial.println(batteryPercent);
    String message = "bt";
    message += batteryPercent;
    bool sent = false;
    while(!sent){
        sent = transmit('H', message);
        if(sent) break;
        delay(500);
    }
    return sent;
}
    
bool MeshManager::transmit(char messageType, String message){

    if(messageType == 'H' && message == "") message = arduinoID;
    else message = arduinoID + "," + message;
    message += " "; // padding so we dont lose data during the transmission
    Serial.println("Message: " + message);
    message.toCharArray(payload, message.length());

    Serial.println("Sending: " + message);

    if (!mesh.write(payload, messageType, 33)) {
        fail_count ++;
        if(fail_count > 4){
            Serial.println("Failure threshold reached, restarting.");
            delay(500);
            restart(); // if we fail to send 10 times, restart the node.
        } 
        else{
            // If a write fails, check connectivity to the mesh network
            if ( !mesh.checkConnection() ) {
                //refresh the network address
                Serial.println("Renewing Address");
                mesh.renewAddress();
                return false;
            } else {
                Serial.println("Send fail, Test OK");
                return false;
            }
        }
    } else {
        Serial.println("Send OK");
        fail_count = 0; // resets fail count
        return true;
    }
    memset(payload, 0, sizeof payload); // wipes the contents of the payload array
}
