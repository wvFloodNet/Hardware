/**
 *\section Introduction
 *  The settings file allows the user to specify a few operating parameters without
 *  editing the source code of the sensor nodes.
 *
 *\section wait
 *  The wait option sets the time between data transmissions in milliseconds. Data transmissions
 *  occur when the sensor node is in the awake state, meaning that the base station has detected
 *  possible flood conditions and told the sensor nodes to actively collect data.
 *
 *\section heartBeatTime
 *  The heartBeatTime sets the time between heartbeats in milliseconds. Heartbeats occur when the
 *  the sensor node is in the sleep state, meaning that there are no pending flood conditions and
 *  the sensor node is not actively collecting data. The heartbeat is used to ensure the sensor node
 *  is still connected to the mesh network and to let the base station know the node is still alive.
 */

uint32_t wait = 60000;           // number of milliseconds between data transmissions
uint32_t heartBeatTime = 300000;  // number of milliseconds between heartbeats
//uint32_t heartBeatTime = 3000;