/**
 * \section Introduction
 *        The MeshManager library is an abstraction layer for handling all
 *        mesh network related activities. It builds upon the RF24, RF24Network,
 *        and RF24Mesh libraries built by Github user nRF24. Please note that the
 *        newest release of each of the aforementioned libraries were not used for
 *        compatibility reasons.
 *
 *  \author Patrick Shinn
 */

#include <Arduino.h>
#include <RF24.h>
#include <RF24Network.h>
#include <RF24Mesh.h>
#include <SPI.h>
#include <OneWire.h>

class MeshManager{
    private:
        int nodeID = 255;           // initial node ID value, will change upon node boot.
        char payload[33];           // payload placeholder
        int fail_count = 0;
        String arduinoID;

        RF24 radio = RF24(9, 10);
        RF24Network network = RF24Network(radio);
        RF24Mesh mesh = RF24Mesh(radio, network);

         /**
         * Converts a byte into a hexadecimal string.
         * 
         * @param b
         *        byte data to convert
         *        
         * @return String hex of byte
         */
        String getHex(byte b);

        /**
         * Gets the unique hardware identifier from the DS2401.
         * 
         * @param DSpin
         *        Pin connected to the DS4201
         * @return String identifier
         */
        String getIdentifier(int DSpin);

        /**
        * Releases the address assigned to the current Node ID
        */
        void releaseAddress();

        /**
         * Requests a new node ID from the RPi
         */
        void getAddress();
    
    public:

        /**
         * Starts the mesh network connection. This method is responsible for 
         * getting the arduino's ID, connecting to the mesh network, getting a new Node ID
         * and assigning the new Node ID to the arduino.
         * 
         * @param IDpin
         *        Input pin connected to the DS4201 Digital Serail Number.
         */
        void begin(int IDpin);

        /**
         * Returns the Node ID of the sensor unit.
         * 
         * @return int NodeID
         */
        int getNodeID();

        /**
         * Gets whether or not we have a message available to the arduino.
         */
        bool available();

        /**
         * Updates the mesh network configuration. Makes a call to RF24Mesh.update().
         */
        void update();

        /**
         * Reads a message from the radio buffer.
         * 
         * @param header
         *        RF24NetworkHeader to store the message header info in.
         * @param message
         *        Storage for the retrieved message
         * @param size
         *        Number of bytes to read from the radio buffer
         */
        void read(RF24NetworkHeader& header,void * message, int size);

        /**
         * Transmits a heartbeat to the base station.
         */
        bool heartbeat();

        /**
         * Transmits a heartbeat along with the current battery power level.
         */ 
        bool heartbeat(int batteryPercent);

        /**
         * Transmits data to the base station. The message is expected in the following format: [data code][data]
         *
         * The following data codes are supported:
         * 1. us = ultra sonic sensor data (int)
         * 2. tp = temperature data (float)
         * 3. hm = humidity data (float)
         * 4. wp = water presence data (bool)
         * 5. wt = water temperature (int)
         * 6. er = error codes, a list of numbers separated by commas (int)
         *
         * An example for the data 12 coming from an ultrasonic sensor would be: "us12"
         * To send more that one data point, simply separate each datum with a comma.
         * An example of including a temperature of 15 along with the ultrasonic senor data would be:
         * "us12,tp15". Please note that order does not matter, what does matter though is that there are no
         * spaces in the message and that the message does not begin or end with a comma.
         *
         * @param messageType
         *        char message type for header
         * @param message
         *        String message to send
         * @return bool successful message transmission
         */
        bool transmit(char messageType, String message);  

        /**
         * Restarts the node.
         */ 
        void(* restart) (void) = 0; 
};