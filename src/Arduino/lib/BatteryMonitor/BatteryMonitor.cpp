#include<BatteryMonitor.h>

BatteryMonitor::BatteryMonitor(float min_volts, float max_volts, int data_pin){
    min_voltage = min_volts;
    max_voltage = max_volts - min_voltage;
    monitor_pin = data_pin;
}

float BatteryMonitor::get_voltage(){
    int read_voltage = analogRead(monitor_pin);

    float voltage = read_voltage * (5.0/1023.0); // arduino max input voltage over max analog value
    return voltage;
}

float BatteryMonitor::get_percent_left(){
    float current_voltage = get_voltage() - min_voltage;
    float percent = current_voltage/max_voltage;
    if(percent > 1.0) percent = 1.0;
    return percent;
}
