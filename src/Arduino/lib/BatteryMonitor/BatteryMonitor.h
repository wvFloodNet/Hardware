/**
 * \section Introduction
 * 	This is a battery monitor class for Arduino prototyping boards.
 * 	This library was designed to work out of the box with batteries 
 * 	that are 5 volts or under. You can map the 5 volt values
 * 	to a higher voltage on your own.
 *
 * \author Patrick Shinn
 */ 

#include<Arduino.h>

class BatteryMonitor
{
    private:
        float min_voltage, max_voltage;
        int monitor_pin;
    public:
        /**
         * Default constructor, allows the user to define the operating parameters of the battery on a 5 volt scale
         * and define the desired input pin. The input must be an analog input.
         *
         * @param min_voltage
         *        The minimum operating voltage of the battery.
         * @param max_voltage
         *        The maximum charge voltage of the battery.
         * @param monitor_pin
         *        The input pin used to monitor the voltage. 
         *		  This pin must be an analog input.
         */
        BatteryMonitor(float min_voltage, float max_voltage, int monitor_pin);

        /**
         * Gets the current voltage of the battery
         * 
         * @return float current voltage on a 5 volt scale.
         */ 
        float get_voltage();

        /**
         * Gets the current percentage of the battery that remains.
         * 
         * @return float calculated usable percent of battery remaining.
         */ 
        float get_percent_left();

};
