/** 
 *  \file
 *  \section Introduction 
 *        This unit is equipt with dual water presence sensors and a water proof temperature sensor. This unit is designed
 *        to be placed in flood prone areas low to the ground. The unit will report when the area it flooding. The optimal
 *        deployment location for this unit is on stairs, in basements, or other similar locations.
 *        
 *  \author Patrick Shinn
 */

#include <Arduino.h>
#include <RF24Network.h>
#include <MeshManager.h>
#include <Settings.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// temp sensor setup
#define ONE_WIRE_BUS 2 // Data wire is plugged into port 2 on the Arduino
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
  
// water sensors
const int sensor0 = A4, sensor1 = A5;
int disjoint = 0;           // false positive detection value

uint32_t displayTimer = 0;  // clock
bool sleep = false;         // sleep state of the node
String message;             // message placeholder.
MeshManager mesh;           // handles all mesh communications


// Prototype functions, defined in the helper function section below loop.
String messageBuilder(boolean sens0, boolean sens1, float temp);

/**
 * The setup function is responsible for initializing tha sensor unit.
 * Here we setup the radio, connect to the mesh, and setup the 
 * sensors.
 */
void setup(void){
  Serial.begin(115200);
  Serial.println("Starting Model I...");
  // Start up the water temp sensor
  sensors.begin();

  mesh.begin(5);
  displayTimer = millis(); // ensures we have delay before our first sensor data transmission.
}

/*
 * Main function, get and show the temperature
 */
void loop(void){ 
  // checking for changes in the mesh network
  mesh.update();

  if(!sleep){
    // Send to the master node every second
    if (millis() - displayTimer >= wait) {
      displayTimer = millis();
      // getting water sensor values
      bool sens0 = false, sens1 = false;
    
      if(analogRead(sensor0) >= 550) sens0 = true;  // checking to see if the sensors are submerged.
      if(analogRead(sensor1) >= 550) sens1 = true;
     
      // call sensors.requestTemperatures() to issue a global temperature 
      sensors.requestTemperatures(); // Send the command to get temperatures
      float temp = sensors.getTempCByIndex(0); // gets the temp
     
      message = messageBuilder(sens0, sens1, temp);
      mesh.transmit('M', message);
    }
  }else{
    // if we are not actively transmitting, then we need to send a heartbeat
    if(millis() - displayTimer >= heartBeatTime){
      displayTimer = millis();
      mesh.heartbeat();
    }
  }
  
  // listening for sleep and wake messages from the base station.
  while(mesh.available()){
   RF24NetworkHeader header;
      unsigned int data;
      mesh.read(header, &data, sizeof(32));
      Serial.print("Message type: ");
      Serial.println((char)header.type);
      if((char)header.type == 'S'){
        sleep = true;     // sleep message recieved
        Serial.println("Sleeping node...");
      }
      else if((char)header.type == 'W'){
        sleep = false; // wake message recievd
        Serial.println("Waking node...");
      }
  }
}

//################################# Helper Functions ########################################

/**
 * Constructs the messages to be transmitted over the radio.
 * 
 * @param sens0
 *        boolean value indicating wether or not the sensor is reporting water.
 * @param sens1
 *        boolean value indicating wether or not the sensor is reporting water.
 * @param temp
 *        float value for the measured temperature in celcius/
 * @return String message to transmit.
 */
String messageBuilder(boolean sens0, boolean sens1, float temp){
  String message = "";
  String error = "";

  // getting the pressence of water
    if(sens0 && sens1){ // if both sensors pick up water
      message += "wp1";
      disjoint = 0;
    }else{
      if(sens0 || sens1){ // one sensor is picking up water and the other is not.
        disjoint ++;
        if(disjoint >= 5) error += ",er5";
      }else{
        message += "wp0"; // if no water is detected.
      }
    }
    
  // getting water temp
  if(temp == -127.00 ){
    error += ",er6";
  }else{
    message += ",wt";
    message += String(temp, 2);
  }
  
  // final string building
  message += error;
  message += " " ;
  return message;
}
