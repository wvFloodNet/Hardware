/**
 *  \file
 *  \section Introduction
 *        This code is identical to the sensor base code except it only sends heartbeats
 *        to the base station. It collects no data and serves only as a relay station for
 *        the other sensor nodes.
 *
 *  \author Patrick Shinn
 */

#include <Arduino.h>
#include <RF24Network.h>
#include <MeshManager.h>
#include <BatteryMonitor.h>
#include <Settings.h>


uint32_t displayTimer = 0;  // clock
bool sleep = false;         // sleep state of the node
String message;             // message placeholder.
MeshManager mesh;           // handles all mesh communications
BatteryMonitor battery(2.5, 4.2, A0);   // handles reading battery information

/**
 * The setup function adds the node to the mesh and requests a new Node ID.
 * This is the base model, so there is no other functionality.
 */
void setup(void){
  
  // start serial port for debugging
  Serial.begin(115200);
  Serial.println("Starting");
  mesh.begin(5);
  displayTimer = millis(); // ensures we have delay before our first sensor data transmission.
}

/**
 * The Main function relays messages from other nodes as well as
 * reports it's own heartbeats.
 */
void loop() {
  mesh.update();
  // This section relays heartbeat information while the node is sleeping.
  if(millis() - displayTimer >= heartBeatTime){
    displayTimer = millis();
    int batterLevel = battery.get_percent_left() * 100;
    mesh.heartbeat(batterLevel);
    
  }

  // listening for sleep and wake messages from the base station.
  while(mesh.available()){
    Serial.println("Message received!");
    RF24NetworkHeader header;
    unsigned int data;
    mesh.read(header, &data, 32);
    Serial.print("Message type: ");
    Serial.println((char)header.type);
    if((char)header.type == 'S'){
      sleep = true;     // sleep message received
      Serial.println("Sleeping node...");
    }
    else if((char)header.type == 'W'){
      sleep = false; // wake message received
      Serial.println("Waking node...");
    }
  }
}