#! python
# please note that the above line is proper and is needed for distutils.
"""
This file is responsible for running the base station and implements the overall
control flow of the base station described in figure 1 of the home page.

"""

from base_station.controls import MeshManager
from base_station.utilities import *
from time import sleep
import schedule
import logging.config
import configparser


# most of these values are set to None simply to appease Sphinx, the documentation generator.
URL = "https://wvflood.duckdns.org/"  # API URL

pi_number = None       # pi number for the database entry.
dark_sky_key = None    # retrieves the darksky key.
mesh_manager = None    # node manager for communicating with the nodes.

latitude = None        # estimated latitude and longitude of the base station
longitude = None

# configure the logger settings
logging.config.fileConfig("/home/pi/base_station/logging_config.ini", disable_existing_loggers=False)
logger = logging.getLogger(__name__)


def check_weather(key: str, lat: float, long: float) -> None:
    """
    Checks the current weather conditions and determines
    whether or not to start the sensor network.

    :param key: Darksky api key.
    :type key: str
    :param lat: latitude of the base station.
    :type lat: float
    :param long: longitude of the base station.
    :type long: float
    :return: None
    """

    flooding = possible_flooding(key, lat, long)

    if flooding and mesh_manager.sleeping:
        logger.info("Waking network...")
        awake = mesh_manager.wake_network()
        logger.info("Network wake success: {}".format(awake))

    elif not flooding and not mesh_manager.sleeping:
        logger.info("Sleeping network...")
        sleeping = mesh_manager.sleep_network()
        logger.info("Network sleep success: {}".format(sleeping))


# Main program
def main() -> None:
    """
    Main program. Establishes the base station and is responsible for collecting data from
    the sensor nodes and reporting it back to the REST API.

    :return: None
    """

    # allows us to edit variables in the outer scope.
    global pi_number, dark_sky_key, mesh_manager, latitude, longitude

    # loading the config file
    config = configparser.ConfigParser()
    config.read("/home/pi/base_station/config.ini")

    reconnect_time = int(config["Base Station"]["reconnect_time"])

    # check to see if the user gave us a latitude and longitude
    if config["Base Station"]["latitude"] != "None":
        latitude = config["Base Station"]["latitude"]
        longitude = config["Base Station"]["longitude"]
    else:  # try to estimate it
        logger.info("No user specified latitude and longitude, using estimated values from Google Maps")
        latitude, longitude = get_coordinates()

    heartbeat_timeout = int(config["Base Station"]["heartbeat_timeout"])
    dark_sky_key = config["Base Station"]["dark_sky_key"]

    pi_number = get_serial()  # pi number for the database entry.
    mesh_manager = MeshManager(pi_number, heartbeat_timeout, URL)

    logger.info("Booting system.")
    logger.info("RPi Info:")
    logger.info("==========================================================")
    logger.info("Serial: {}".format(pi_number))  # RPi serial info
    logger.info("Approximate Location: {}, {}".format(latitude, longitude))
    logger.info("Darksky key: {}".format(dark_sky_key))
    logger.info("==========================================================")

    # todo finalize time
    # this sets a the check weather job to run 15 min, since this is at start we also run this job immediately.
    schedule.every(15).minutes.do(check_weather, dark_sky_key, latitude, longitude)
    schedule.run_all()

    while True:
        try:
            logger.info("Connecting to API...")
            api_connected = api_reachable(URL)
            if api_connected:
                logger.info("Connected!")
                while api_connected:
                    mesh_manager.set_connected(api_connected)
                    schedule.run_pending()                   # if an hour has passed, check the weather again
                    api_connected = mesh_manager.run_mesh()  # relay messages from the mesh.
            else:
                # waiting for an active network connection to reestablish
                logger.warning("Failed, API could not be reached!")
                logger.warning("No network connection... retry in {} seconds".format(reconnect_time))
                sleep(reconnect_time)
        except Exception as error:
            logger.critical("Exception occurred:", error)


if __name__ == "__main__":
    main()
