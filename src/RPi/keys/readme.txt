All api keys should be stored in this folder. They should follow
the naming convention of apiName.key. To access this key using
the get_api_key method in Base_Station.utilities, simple pass
the value given as apiName.