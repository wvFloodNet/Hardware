"""
The utilities module contains a collection of
utility functions that make operating the base
station easier, and the code base a little cleaner.
"""

from urllib.request import urlopen
from urllib.error import URLError
from darksky import forecast
import logging
from typing import Union

# module logger settings
logger = logging.getLogger(__name__)


def get_coordinates() -> tuple:
    """
    Using the Google maps web interface, we extract the
    estimated location of the base station. This is not
    an exact location and is only used to obtain a weather
    forecast.

    :return: (float) latitude, (float) longitude
    :rtype: (float, float)
    """
    response = urlopen('https://www.google.com/maps/', timeout=5)
    response = str(response.read())
    response = response.strip("b'")  # remove bytes notation
    response = response.split(">")  # splitting on all html/xml tags

    # here we find the index of the url that contains the latitude and longitude
    index = 0
    for element in response:
        if not element.__contains__("meta content=\"https://maps.google.com"):
            index += 1
        else:
            break
    # now we split off all the unneeded data and extract the lat and long.
    data_url = response[index]
    data_url = data_url.split("?")
    data = data_url[1]
    data = data.split("%2C")
    lat = data[0].split("=")[1]
    long = data[1].split("&")[0]
    return lat, long


def possible_flooding(key: str, lat: float, long: float) -> bool:
    """
    Using the Darksky Weather API, check to see if it is going to possible
    flood. This is done by checking the current weather and the weather for
    the next hour. If it is going to rain for either of those, we return true.
    Rain is not the only condition for flooding however, we also check to see
    if we have a flooding alert as well. If any of there are true, we consider
    the possibility of flooding to be true.

    :param key: darksky api key.
    :type key: str
    :param lat: latitude of the base station.
    :type lat: float
    :param long: longitude of the base station.
    :type long: float
    :return: possible flooding.
    :rtype: bool
    """

    weather = forecast(key, lat, long, exclude=["daily, minutely"])
    current = weather.currently.icon
    next_hour = weather.hourly.icon

    # return True # this is for testing purposes
    try:
        alert = str(weather.alert.title)
    except AttributeError:
        alert = "No alerts"

    if current == "rain" \
            or current == "thunderstorm"\
            or next_hour == "rain"\
            or next_hour == "thunderstorm"\
            or "flood" in alert.lower():
        return True
    else:
        return True  # todo make this false


def api_reachable(url: str) -> bool:
    """
    Checks to see if out REST API is reachable.

    :param url: api URL.
    :type url: str
    :return: reachable.
    :rtype: bool
    """
    try:
        urlopen(url, timeout=5)  # connects to a REST API server
        return True
    except URLError:
        return False


def get_serial() -> str:
    """
    Gets the serial number of the RPi from the /proc/cpuinfo file.

    :return: serial number of Raspberry Pi.
    :rtype: str
    """
    try:
        f = open('/proc/cpuinfo', 'r')  # file containing pi serial number
        for line in f:
            if line[0:6] == 'Serial':
                return line[10:26]      # section of line containing the serial number
        f.close()
    except FileNotFoundError:
        logger.critical("No serial number found.")
        return "Error"
