"""
File contains all classes related to base station management. The classes contained are:
    1. Mesh Manager: responsible for overall node management.
    2. Node ID Manager: helper class for the node manager, manages how Node IDs are leased.
    3. Node Data: a data wrapper class for easy information access and storage.
    4. API: this class is an abstraction layer for communicating with the REST API.

"""

import pickle                           # for writing out objects to a file
import threading                        # this is used to access a thread locking mechanism
from _thread import start_new_thread
from time import time, sleep
import requests
import json
import logging
from typing import Union, Dict, Type

from struct import pack                 # used in data transmission

import RPi.GPIO as GPIO                 # the remaining are all NRF24 required libs.
from RF24 import *
from RF24Network import *
from RF24Mesh import *


# module logger settings
logger = logging.getLogger(__name__)


class MeshManager:
    """
    This class is responsible for handling all communication between the base station and
    the sensor nodes as well as relaying the data to the REST API.
    """

    def __init__(self, base_id: str, heartbeat_timeout: int, api_url: str) -> None:
        """
        Initializer for the NodeManager object.

        :rtype: None
        """
        self.node_manager = NodeManager(heartbeat_timeout)
        self.radio = RF24(RPI_V2_GPIO_P1_15,            # NRF24 radio
                          RPI_V2_GPIO_P1_24,
                          BCM2835_SPI_SPEED_8MHZ)
        self.network = RF24Network(self.radio)          # NRF24 Network
        self.mesh = RF24Mesh(self.radio, self.network)  # NRF24 Mesh

        self.mesh.setNodeID(0)                          # establish the pi as the master node
        self.mesh.begin()                               # setup the network
        self.radio.setPALevel(RF24_PA_MAX)              # Power Amplifier
        self.sleeping = False
        self.base_id = base_id
        self.api = API(api_url, base_id)

    def assign_node_id(self, payload: str) -> None:
        """
        Assigns an unused node id to a new node being added to the network.

        :param payload: arduino payload
        :type payload: str
        :return: None
        """

        logger.info("New Node Detected")
        hardware_id = payload.split(',')[0]
        lease = self.node_manager.request_node_id(hardware_id)
        logger.info("Issued Node ID: {}".format(lease))
        logger.info("Successful issuing: {}".format(self.transmit('A', lease, 255)))

    def transmit(self, message_type: str, message: int, recipient: int) -> bool:
        """
        Transits a message to a specified recipient.
        This method will try to send a message to the
        target node 6 times, after which it will stop.

        :param message_type: message type.
        :type message_type: char
        :param message: message to be transmitted.
        :type message: int
        :param recipient: node id of the node to send to.
        :type recipient: int
        :return: success value of the transmission.
        :rtype: bool
        """

        if not self.mesh.write(pack('I', message), ord(message_type), recipient):
            retry = 0
            while retry < 5:
                done = self.mesh.write(pack('I', message), ord(message_type), recipient)
                retry += 1
                if done:
                    return True  # if we succeed within the 6 time limitation, return true
                sleep(0.1)
        else:
            return True  # if we succeed on the first go, we return true
        return False  # if we break out without successful acknowledgement, return false

    def broadcast(self, message_type: str, message: int) -> bool:
        """
        Broadcasts a message to all nodes on the network, alive or not.
        The broadcast fails only if it fails to receive an acknowledgement
        from any of the living nodes. Living nodes are determined by comparing
        the time of their last heartbeat to the current time and checking whether
        or not the difference is within the heartbeat timeout range.

        :param message_type: message type, this should be a single character code.
        :type message_type: str
        :param message: message to be transmitted.
        :type message: int
        :return: The success value of the broadcast.
        :rtype: bool
        """
        successful_broadcast = True  # place holder value, assume true to start.
        nodes = self.node_manager.get_all_nodes()
        living_nodes = self.node_manager.get_living_nodes()
        logger.info("Known nodes: {}".format(nodes.keys()))
        logger.info("Living nodes: {}".format(living_nodes))
        for node in nodes:
            logger.info("Broadcasting to: {}".format(node))
            if not self.transmit(message_type, message, nodes[node].node_id):
                if node in living_nodes:  # check to see if the failed node is considered alive
                    # if even one nodes fails, consider the broadcast to fail
                    logger.warning("Failed to send")
                    successful_broadcast = False
            else:
                logger.info("Successfully sent")
                if node not in living_nodes:  # this updates the heartbeat and adds a living node.
                    self.node_manager.heartbeat_received(node)

        return successful_broadcast

    def sleep_network(self) -> bool:
        """
        Puts all nodes on the network to sleep by broadcasting a sleep message.

        :return: whether or not the network was put to sleep successfully.
        :rtype: bool
        """
        self.sleeping = True
        return self.broadcast('S', 0)

    def wake_network(self) -> bool:
        """
        Wakes all nodes on the network by broadcasting a wake message to all nodes.

        :rtype: bool
        :return: awake
        """
        self.sleeping = False
        return self.broadcast('W', 0)

    def run_mesh(self) -> bool:
        """
        Actively collects information from the sensors.

        :rtype: bool
        :return: Data successfully uploaded to the REST API or not.
        """
        self.mesh.update()  # read any changes to the network
        self.mesh.DHCP()    # handles address changes

        while self.network.available():
            header, payload = self.network.read(32)

            # Converts the payload to a string, removing all spaces
            payload = payload.decode("utf-8").replace("\x00", "")
            hardware_id = payload.split(',')[0]

            if len(hardware_id) > 12:  # handles an issue where there is a missing separator
                hardware_id = hardware_id[0:13]
                payload = payload.strip(hardware_id)
                payload = hardware_id + "," + payload  # reformat the payload to have the separator

            logger.debug(payload)
            logger.debug(hardware_id)

            # handles instance where this is the first time we have seen this node
            try:
                mode = self.node_manager.get_node(hardware_id).get_mode()
            except KeyError:
                mode = 'a'

            if self.mesh.getNodeID(header.from_node) == 255:
                # launches the assign node method as a thread, allowing us to continue getting data while addressing.
                # the comma after payload is to create a one element tuple.
                start_new_thread(self.assign_node_id, (payload,))
                start_new_thread(self.api.send_data, (payload,))

            elif chr(header.type) == 'M':  # checks the message type
                if self.sleeping:  # if the network should be asleep but the node isn't, put it to sleep
                    if mode == 'm':  # if manual mode is enabled, leave the node awake
                        self.send_data(payload, hardware_id, header)
                    else:  # if we are in automatic mode, sleep the node
                        try:
                            self.transmit('S', 0, self.mesh.getNodeID(header.from_node))
                        except OverflowError:  # recovers from a corrupt package
                            self.bad_send_recovery(hardware_id, 'S')
                else:
                    self.send_data(payload, hardware_id, header)

                # updates the heartbeat locally
                start_new_thread(self.node_manager.heartbeat_received, (hardware_id, ))

            elif chr(header.type) == 'H':  # heartbeat message, so only update heartbeat
                if not self.sleeping or mode == 'm':      # if the network should not be asleep but the node is, wake it
                    try:
                        self.transmit('W', 0, self.mesh.getNodeID(header.from_node))
                    except OverflowError:  # recovers from a corrupt package
                        self.bad_send_recovery(hardware_id, 'W')

                self.send_data(payload, hardware_id, header)

                # updates local heartbeat count
                start_new_thread(self.node_manager.heartbeat_received, (hardware_id, ))

            else:  # errors out if message type is unexpected
                logger.warning("Rcv bad type {} from 0{:o}".format(header.type, header.from_node))

        return self.api.connected

    def set_connected(self, connected: bool) -> None:
        """
        Sets api_connected attribute, this is primarily used for the control flow of the base station.

        :param connected: truth value of the connection
        :type connected: bool
        :rtype: None
        :return: None
        """
        self.api.connected = connected

    def sync(self) -> None:
        """
        Updates all node metadata to what is stored in the REST API.

        :return: None
        """
        node_metadata = self.api.get_node_metadata()
        self.node_manager.sync(node_metadata)

    def bad_send_recovery(self, hardware_id: str, message_code: str) -> None:
        """
        Attempts to send a message to a node who has reported a bad NodeID value of -1.
        This bad value is typically caused by interference on the network resulting
        in a corrupt package. The method attempts to overcome this issue by looking
        up the NodeID of the hardware_id in question and transmitting to it.

        :param hardware_id: hardware_id of node.
        :type hardware_id: str
        :param message_code: message code to transmit
        :type message_code: str
        :return: None
        """

        logger.critical(
            "Hardware_id: {} reported a bad NodeID, this is most likely just a bad transmission."
            "".format(hardware_id))
        logger.info("Looking up stored NodeID for {} and using it.".format(hardware_id))
        node_data = self.node_manager.who_is_hardware_id(hardware_id)
        logger.info("Successful recovery: {}".format(self.transmit(message_code, 0, node_data.node_id)))

    def send_data(self, payload: str, hardware_id: str, header) -> None:
        """
        Using the API class, the current payload is uploaded to the REST API.

        :param payload: payload data.
        :type payload: str
        :param hardware_id: node hardware_id.
        :type hardware_id: str
        :param header: RF24Network header.
        :type header: RF24Network.header.
        :return: None
        """
        logger.info("Rcv {} from Address: {},"
                    "NodeID: {},"
                    "HardwareID: {}".format(payload,
                                            header.from_node,
                                            self.mesh.getNodeID(header.from_node),
                                            hardware_id
                                            ))
        start_new_thread(self.api.send_data, (payload,))


class NodeManager:
    """
    NodeManager class

    This class is used for managing the leased addresses for the NRF Mesh,
    allowing dynamic Node IDs to be leased to each sensor node instead of forcing
    them to be hard coded in the firmware. It also handles other node data
    like when the last heartbeat was.
    """

    def __init__(self, heartbeat_timeout: int, number_of_nodes=254) -> None:
        """
        Initializer for the NodeIDManager object.

        :param number_of_nodes: number of Node IDs that can be leased. Defaults to 254. Address 255 is reserved
        :type number_of_nodes: int
        :param heartbeat_timeout: time in seconds to consider a sensor alive without a heartbeat.
        :type heartbeat_timeout: int
        :rtype: None
        """
        logger.info("Node Manager Startup")
        logger.info("==========================================================")
        try:  # first we attempt to load settings that persist across reboots
            with open('NodeDictionary.txt', 'rb') as node_dictionary:
                self.nodes = pickle.load(node_dictionary)
                node_dictionary.close()
            with open('NodeList.txt', 'rb') as node_list:
                self.node_ids = pickle.load(node_list)
                node_list.close()
            logger.info("Node settings successfully restored.")
            logger.info("==========================================================")
        except FileNotFoundError:  # if that fails, start fresh
            self.nodes = dict()
            if number_of_nodes > 254:
                self.node_ids = [None] * 253
                logger.warning("There is a max of 254 Node IDs available, number of nodes has been set to this.")
            elif number_of_nodes < 1:
                self.node_ids = [None]
                logger.warning("You must have at least one Node ID")
            else:
                self.node_ids = [None] * (number_of_nodes - 1)  # this compensates for the +1 offset so that no ID is 0.
            logger.info("No settings found for Node IDs, assuming fresh deployment.")
            logger.info("==========================================================")
        self.lock = threading.Lock()
        self.heartbeat_timeout = heartbeat_timeout

    def request_node_id(self, arduino_id: str) -> Union[int, None]:
        """
        Requests a Node ID that is not currently in use by any
        other sensor node. This method is thread safe.

        :param arduino_id: unique arduino identifier
        :type arduino_id: string
        :return: Node ID.
        :rtype: int
        """
        self.lock.acquire()  # prevents more than one ID from being issued at once

        logger.info("Arduino ID is: {}".format(arduino_id))
        if arduino_id in self.nodes:
            logger.info("This unit has already receive Node ID, reissuing same ID.")
            self.lock.release()
            return self.nodes[arduino_id].node_id
        else:
            for node_id in range(len(self.node_ids)):
                if self.node_ids[node_id] is None:  # here we are just looking for the first available Node ID.
                    try:
                        # keeps track of which addresses are available.
                        self.node_ids[node_id] = arduino_id
                        # keeps track of who already has been addressed.
                        self.nodes[arduino_id] = NodeData(node_id + 1, time())

                        # immediately save node addressing changes to a text file
                        with open("NodeDictionary.txt", 'wb') as node_dictionary:
                            pickle.dump(self.nodes, node_dictionary)
                            node_dictionary.close()
                        with open("NodeList.txt", "wb") as node_list:
                            pickle.dump(self.node_ids, node_list)
                            node_list.close()
                        self.lock.release()
                        return node_id + 1  # we cannot have a Node ID of zero, so just offset all ids by 1.
                    except IndexError:
                        logger.critical("There are no more Node IDs available.")
                        self.lock.release()
                        return None

    def get_node(self, hardware_id: str) -> 'NodeData':
        """
        Gets a specified node by hardware_id.

        :param hardware_id: unique hardware_id
        :type hardware_id: str
        :rtype: NodeData
        :return: NodeData object for specified node.
        """

        return self.nodes[hardware_id]

    def get_all_nodes(self) -> Dict[str, 'NodeData']:
        """
        Gets the dictionary of arduino/node ID pairs

        :return: the dictionary of arduino IDs.
        :rtype: Dict[str, NodeData]
        """
        return self.nodes

    def get_living_nodes(self) -> Dict[str, 'NodeData']:
        """
        Returns a dictionary of all nodes that have reported
        a heartbeat within the heartbeat_timeout time.

        :return: Living Nodes dictionary.
        :rtype: dict
        """
        living_nodes = dict()
        now = time()
        for node in self.nodes:
            if now - self.nodes[node].last_heartbeat < self.heartbeat_timeout:  # 2400 is 40 minutes in seconds.
                living_nodes[node] = self.nodes[node].node_id
        return living_nodes

    def release_node_id(self, node_id: int) -> None:
        """
        Releases a node id. This method is thread safe.

        :param node_id: Node ID to be released
        :type node_id: int
        :return: None
        """
        self.lock.acquire()
        del self.nodes[self.node_ids[node_id - 1]]
        self.node_ids[node_id - 1] = None
        # immediately save node addressing changes to a text file
        with open("NodeDictionary.txt", 'wb') as node_dictionary:
            pickle.dump(self.nodes, node_dictionary)
            node_dictionary.close()
        with open("NodeList.txt", "wb") as node_list:
            pickle.dump(self.node_ids, node_list)
            node_list.close()
        self.lock.release()

    def who_has_id(self, node_id: int) -> Union[str, None]:
        """
        Returns the arduino number the Node ID is leased to.

        :param node_id: Node ID of the arduino in question.
        :type node_id: int
        :return: Arduino Number.
        :rtype: str
        """
        return self.node_ids[node_id - 1]

    # due to language restrictions, the annotation here does not help the IDE with autocomplete
    def who_is_hardware_id(self, hardware_id: str) -> Union['NodeData', None]:
        """
        Returns the Node ID of an arduino number.

        :param hardware_id: arduino number.
        :type hardware_id: str
        :return: Node metadata attached to hardware_id.
        :rtype: NodeData
        """
        try:
            return self.nodes[hardware_id]
        except KeyError:
            logger.critical("'{}' has not been registered.".format(hardware_id))
            return None

    def heartbeat_received(self, hardware_id: str) -> None:
        """
        Updates the heartbeat time for the specified arduino.
        This method is thread safe.

        :param hardware_id: arduino number.
        :type hardware_id: str
        :return: None
        """
        self.lock.acquire()
        try:
            self.nodes[hardware_id].update_heartbeat(time())
            with open("NodeDictionary.txt", 'wb') as node_dictionary:
                pickle.dump(self.nodes, node_dictionary)
                node_dictionary.close()
        except KeyError:
            logger.critical("Key {} not found".format(hardware_id))
        self.lock.release()

    def sync(self, node_metadata: dict) -> None:
        """
        Updates the nodeData to match the most recent data from the REST API
        :param node_metadata: node metadata from the REST API
        :type node_metadata: dict
        :return: None
        """

        for key in node_metadata.keys():
            self.nodes[key] = node_metadata[key]


class NodeData:
    """
    Data wrapper for node information.
    """

    def __init__(self, node_id: int, last_heartbeat: float, mode='a') -> None:
        """
        Initializer for NodeData

        :param node_id: node id issued to this node.
        :type node_id: int
        :param last_heartbeat: last heartbeat time in utc.
        :type last_heartbeat: float
        :param mode: operation mode code for the node.
        :type mode: str
        :rtype: None
        :return: None
        """
        self.node_id = node_id
        self.last_heartbeat = last_heartbeat
        self.mode = mode

    def __str__(self) -> str:
        """
        To string method for the node
        :rtype string
        :return: node_id
        """
        return str(self.node_id)

    def update_heartbeat(self, new_heartbeat: float) -> None:
        """
        Updated the last heartbeat time.

        :param new_heartbeat: new heartbeat time in utc.
        :type new_heartbeat: float
        :rtype: None
        :return: None
        """
        self.last_heartbeat = new_heartbeat

    def set_mode(self, mode: str) -> None:
        """
        Sets the mode type for the node.

        :param mode: node operating mode code.
        :type mode: str
        :rtype: None
        """
        self.mode = mode

    def get_mode(self) -> str:
        """
        Gets teh mode type set for the node.

        :rtype str
        :return: node operation mode
        """
        return self.mode


class API:
    """
    Handles all interactions with the REST API.
    """

    def __init__(self, url: str, base_id: str) -> None:
        """
        Default constructor, sets the url for REST API operation

        :param url: desired API url
        :type url: str
        :param base_id: the base station's serial number.
        :type base_id: str
        :rtype: None
        """
        self.url = url
        self.connected = True
        self.base_id = base_id

    def send_data(self, node_data: str) -> None:
        """
        Parses the data transmitted from the sensor nodes and converts it to a json format that can then
        be used by the REST API.


        The format for such data coming from the base sensor nodes would be in the format codeData. For example, an
        ultrasonic sensor reading of 12 would be transmitted as "us12". If the transmitted data does not have a code
        attached to it, or the code does not match any of the supported codes, then that data is assumed to be the
        Arduino ID, the unique identifier for the sensor node.


        :param node_data: sensor data.
        :type node_data: str
        :type: str
        :return: None
        """
        logger.debug("Node Data: {}".format(node_data))
        node_data = node_data.split(",")
        json_data = dict()

        json_data["base_id"] = self.base_id
        hardware_id = node_data[0].strip()
        json_data["node_id"] = hardware_id
        error_list = list()
        dataset = list()
        for x in range(1, len(node_data)):
            code = node_data[x][0:2].strip()
            data = node_data[x][2:len(node_data[x])].strip()
            if code == "er":
                error_list.append(data)
            elif code == "":
                # this is an instance of missing parameter from the nodes, this sometimes happens with error codes
                logger.warning("There was an instance of a missing code, correcting for the issue.")
            else:
                datum = dict()
                datum["sens_typ_cd"] = code
                datum["data"] = data
                dataset.append(datum)
        if not len(dataset) == 0:  # only add data set if we have data
            json_data["dataset"] = dataset
        if not len(error_list) == 0:  # only add error list if we have errors
            json_data["error_list"] = error_list

        logger.debug("Transmitted data: {}".format(json.dumps(json_data)))
        # sending the post request
        request = requests.post(self.url + "/insert", json=json_data)
        # check whether or not we successfully connected to the API
        logger.debug(request.status_code)  # remove debug
        if not 200 == request.status_code:
            logger.critical("Failed to send data to the API: {}".format(node_data))
            self.connected = False
        else:
            logger.debug("Data transmitted successfully: ".format(node_data))
            self.connected = True

    def get_node_metadata(self) -> dict:
        """
        Gets all node metadata from nodes associated with the base station.

        :rtype: dict
        :return: dict(str, nodeData)
        """
        pass
