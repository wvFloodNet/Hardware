"""
This package contains two modules:
    1. base_station_controls
    2. utilities

The base_station_control module handles how the Raspberry Pi communicates with the sensor network,
manages the nodes on the network, and relays information to the REST API.

The utilities module contains several utility functions that make managing the base station a little easier
and makes the code a bit cleaner.
"""
