Welcome!
########################################################################################################################
This package includes all of the necessary software components to convert a Raspberry Pi 3 into a
base station for the West Virginia Flood Network.

To change the style and output of the program log, please edit /home/pi/base_station/logging_config.ini.