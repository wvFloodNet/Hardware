"""
 Setup file for the Base Station code. Installs the base_station package
 and establishes the main file as an executable program callable from the
 command line.
"""

from setuptools import setup

setup(
    name="WV Flood Network Base Station",
    version="1.2.dev1",
    author='Patrick Shinn',
    author_email='shinn16@marshall.edu',
    license="GNU General Public License 2",
    url='https://wvfloodnet.gitlab.io/Hardware/base_station/',
    description="West Virginia Flood Network Base Station drivers",
    platforms=['Raspberry Pi 3 B', 'Raspberry Pi 3 B+'],
    packages=['base_station'],
    scripts=['baseStation.py'],

    install_requires=[
        'schedule',
        'darkskylib'
    ],
    long_description=open('README.txt').read()
)
