#!/usr/bin/env bash
# Author Patrick Shinn
# Email: shinn16@marshall.edu
#
# This script builds the python wrappers for the RF24Mesh library.

echo "################################## Building Python Wrappers for RF24Mesh ########################################"
echo "Building wrappers..."
cd ../RF24Mesh/pyRF24Mesh/
sudo python3 setup.py install
echo "###################################### Done Python Wrappers for RF24 ############################################"
echo ""
echo ""
