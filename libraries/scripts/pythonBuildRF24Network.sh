#!/usr/bin/env bash
# Author Patrick Shinn
# Email: shinn16@marshall.edu
#
# This script builds the python wrappers for the RF24Network library.

echo "############################### Building Python Wrappers for RF24 Network #######################################"
cd ../RF24Network/RPi/pyRF24Network/
sudo python3 setup.py install
echo "################################# Python Wrappers for RF24 Network Done  ########################################"
echo ""
echo ""
