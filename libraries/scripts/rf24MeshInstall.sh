#!/usr/bin/env bash
# Author Patrick Shinn
# Email: shinn16@marshall.edu
#
# This script builds and installs the RF24Mesh library.
echo "################################################# RF24 Mesh #####################################################"
echo "Building RF24Mesh"
cd ../src
unzip RF24Mesh.zip
mv RF24Mesh/ ../
cd ../RF24Mesh
sudo make
sudo make install
echo "############################################### RF24 Mesh Done ##################################################"
echo ""
echo ""
