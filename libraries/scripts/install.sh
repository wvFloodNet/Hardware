#!/usr/bin/env bash
# This is the master installer script for the RPI and must be run as root
# Base station installation script. Handles compiling all needed libraries and installing the base station code.
# Be sure that you run this script from within the base_station directory (cd into it)

current_dir=$(pwd)  # lets us find our way back

# Library installation and compilation
sudo -S cp -r libraries /opt/
cd /opt/libraries
sudo ./rpi_build.sh

# base station package installation
echo "######################################### Installing Python Package #############################################"
cd "$current_dir"
sudo pip3 install *.gz
echo "########################################### Python Package Installed ############################################"
echo ""
echo ""


# creating the system service
echo "########################################## Creating System Service ##############################################"	
sudo cp -v libraries/scripts/basestation.service /etc/systemd/system/
sudo systemctl daemon-reload
echo "reloading systemd..."
sudo systemctl enable basestation.service
echo "enabling basestation.service"
echo "System service created successfuly."
echo "########################################### End Service Creation ###############################################"
echo ""
echo ""

# creating config and log files
echo "########################################### Creating Config Files ###############################################"
echo "Please enter your DarkSky api key, if you do not have one you will need to visit https://darksky.net/dev
and register for an API key. DO NOT INCLUDE SPACES! Please enter your key:>"
read api_key
echo "" >> /home/pi/base_station/config.ini  # ensures we put the dark sky entry on its own line
echo "dark_sky_key = $api_key" >> /home/pi/base_station/config.ini
echo "########################################### Config Files Created ################################################"
echo ""
echo ""

echo "Installation complete, the Raspberry Pi will now reboot in 5 seconds"
sleep 5
sudo reboot
