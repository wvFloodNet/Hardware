#! /bin/bash
# Author Patrick Shinn
# Email: shinn16@marshall.edu
#
# This script builds the python wrappers for the RF24 base library.

version=$(python3 --version | grep -Eho [1-9].[1-9])  # extracts python version number
version=$(echo "$version" | grep -Eho [1-9] | tr '\n' '\0')  # extracts numbers with no period between them

echo "#################################### Building Python Wrappers for RF24 ##########################################"
echo "Downloading additional software for python..."
cd ../RF24-1.3.1/pyRF24
sudo -S apt-get install -y python3-pip python3-dev libboost-python-dev python3-setuptools  # gets all needed software to make python wrapper
sudo pip3 install RPi.GPIO
sudo -S ln -s /usr/lib/arm-linux-gnueabihf/libboost_python-py"$version".so /usr/lib/arm-linux-gnueabihf/libboost_python3.so # link pythonboost to python3 for succesful compilation
sudo python3 setup.py build # build and install python files
sudo python3 setup.py install
echo "###################################### Python Wrappers for FR24 Done ############################################"
echo ""
echo ""
