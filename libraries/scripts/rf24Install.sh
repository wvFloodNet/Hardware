#! /bin/bash
# Author Patrick Shinn
# Email: shinn16@marshall.edu
#
# Builds and installs the base RF24 library.

echo "############################################## BUILDING RF24 ####################################################"
echo "Installing RF24 from source...."
cd ../src
unzip RF24-1.3.1.zip  # just unziping and bulding rf24 from source
mv RF24-1.3.1 ../
cd ../RF24-1.3.1
sudo ./configure --driver=RPi
sudo -S make
sudo -S make install
echo "################################################## DONE RF24 ####################################################"
echo ""
echo ""
