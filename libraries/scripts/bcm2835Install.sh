#! /bin/bash
#Author Patrick Shinn
# Email: shinn16@marshall.edu
#
# This script builds BCM drivers needed to compile the base RF24 library.

echo "############################################ INSTALLING BCM DRIVERS #############################################"
echo "Installing BCM2835 from source..."
cd ../src
tar zxvf bcm2835-1.52.tar.gz # expand the included bcm files
mv bcm2835-1.52 ..
cd ../bcm2835-1.52
./configure # configure for building from source
make # make and install
sudo -S make check
sudo -S make install
echo "################################################# DONE BCM ######################################################"
echo ""
echo ""