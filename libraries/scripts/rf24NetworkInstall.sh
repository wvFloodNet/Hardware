#!/usr/bin/env bash
# Author Patrick Shinn
# Email: shinn16@marshall.edu
#
# This script complies the RF24Network library. The RF24 library must be installed first.
echo "################################################# RF24 Network ##################################################"
echo "Building RF24Network"
cd ../src
unzip RF24Network.zip
mv RF24Network ../
cd ../RF24Network
sudo make
sudo make install
echo "############################################### RF24 Network Done ###############################################"
echo ""
echo ""
