#! /bin/bash
# Author Patrick Shinn
# Email: shinn16@marshall.edu
#
# Master install script for all components of the NRF24 Mesh network. To install individual components,
# use the individual install scripts in the scripts folder.

cd ./scripts/
echo "###################################### Welcome to the WV Flood Net! #############################################"

echo "############################################## Building Libraries ###############################################"
sudo -S apt-get update && sudo apt-get upgrade -y
sudo -S ./bcm2835Install.sh          # builds bcm driver first.
sleep 1
sudo -S ./rf24Install.sh             # builds RF24 using bcm driver. This must be built before RF24Network.
sleep 1
sudo -S ./pythonBuildRF24.sh         # builds python wrapper for RF24.
sleep 1
sudo -S ./rf24NetworkInstall.sh      # builds RF24Network library. This must be built before RF24Mesh.
sleep 1
sudo -s ./pythonBuildRF24Network.sh  # builds python wrappers for RF24Network library.
sleep 1
sudo -s ./rf24MeshInstall.sh         # builds RF24Mesh library.
sleep 1
sudo -s ./pythonBuildRF24Mesh.sh     # builds python wrappers for RF24Mesh library.
echo "################################################# Building Done #################################################"
echo ""
echo ""

