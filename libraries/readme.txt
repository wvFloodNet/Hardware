Author: Patrick Shinn
Email: shinn16@marshall.edu
===============================

This directory contains all of the libraries needed to run this project for both the Arduino Boards and the Raspberry
Pi.

Arduino
===============================
To use the arduino libraries, install the arduino ide. From there, all you need to do is add the library zip files
using the graphic interface provided.

Raspberry Pi
===============================
To use on the RPi, copy the libraries folder to the home folder of the Pi user. Then all you need to do is run the
rpi_build.sh script.

A few of the libraries used for the Pi are a few commits behind master for the RF24 packages. The libraries are not
working at the current time, so below the commits used in this project are listed with their respective library.

|--------------------------------------------------------|
|   Library   |            Commit Number                 |
|-------------|------------------------------------------|
| RF24Network |  a2ce4aee1f94f742018b5b4d50665d9cbfb360b7|
| RF24Mesh:   |  7708cfc9f4cbc615b759401394d973e7d51cd2b5|
|--------------------------------------------------------|

The BCM drivers and the RF24 base library are official releases. Their release versions are reflected in the names
of the libraries.